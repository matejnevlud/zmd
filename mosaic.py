import cv2
import numpy as np
import sys


if __name__ == '__main__':
    args = sys.argv[1:]

    if len(args) != 6:
        print("You must specify correct parameters: rows cols image_width image_height input_video output_image")
        exit(0)

    a_rows, a_cols, a_width, a_height, a_input, a_output = args
    a_rows = int(a_rows)
    a_cols = int(a_cols)
    a_height = int(a_height)
    a_width = int(a_width)

    if a_rows * a_cols < 1:
        print("You must specify at least 1 tile")
        exit(0)

    cap = cv2.VideoCapture(a_input)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    print("FPS:", cap.get(cv2.CAP_PROP_FPS))
    print("Frame count:", frame_count)

    #! Calculate frame steps for each tile linearly
    frame_steps = np.linspace(0, frame_count - 1, a_rows * a_cols).astype(int)
    frame_array = []

    for i in range(0, len(frame_steps)):
        current_frame = frame_steps[i]
        print("Frame:", i, current_frame)

        cap.set(cv2.CAP_PROP_POS_FRAMES, current_frame);
        ret, img_frame = cap.read()
        frame_array.append(img_frame)

    cap.release()


    #! Create mosaic image for stitching
    mosaic_img = np.zeros((a_height, a_width, 3), np.uint8)
    r_width = int(a_width / a_cols)
    r_height = int(a_height / a_rows)

    #! Go througn all frames and put them in correct place in mosaic image
    for i in range(0, a_rows):
        for j in range(0, a_cols):
            k = i * a_cols + j
            r_image = cv2.resize(frame_array[k], (r_width, r_height))

            offset_x = r_width * j
            offset_y = r_height * i

            #! Copy pixels
            for y in range(0, r_height):
                for x in range(0, r_width):
                    mosaic_img[y + offset_y, x + offset_x] = r_image[y, x]

            #cv2.imshow(str('current_frame'), r_image)
            #cv2.waitKey(0)


    #cv2.imshow(str('mosaic'), mosaic_img)
    #cv2.waitKey(0)
    cv2.imwrite(a_output, mosaic_img)
    print("Done")

