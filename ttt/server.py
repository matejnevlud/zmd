import socket
import cv2
import pickle
import struct
#import imutils
from sampler import *


s = Sampler('./sine.wav', Modulation.AMPLITUDE, 44100)
# Server socket
# create an INET, STREAMing socket
server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host_name  = socket.gethostname()
host_ip = socket.gethostbyname(host_name)
print('HOST IP:',host_ip)
port = 10050
socket_address = (host_ip,port)
print('Socket created')
# bind the socket to the host.
#The values passed to bind() depend on the address family of the socket
server_socket.bind(socket_address)
print('Socket bind complete')
#listen() enables a server to accept() connections
#listen() has a backlog parameter.
#It specifies the number of unaccepted connections that the system will allow before refusing new connections.
server_socket.listen(5)
print('Socket now listening')

message = b''

while True:
    client_socket,addr = server_socket.accept()
    print('Connection from:',addr)
    if client_socket:
        message = b''
        for x in range(int(s.length * s.samplerate)):

            time = x / s.samplerate
            i, q = s.iq_val_at(time)

            i, q = struct.unpack('!ff',struct.pack("!ff", i, q))


            message += struct.pack("!ff", i, q)
            if x % 128 == 127:
                client_socket.sendall(message)
                message = b''

        client_socket.sendall(b'0')
        client_socket.close()
