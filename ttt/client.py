import socket
import cv2
import pickle
import struct
#import imutils
from decoder import Decoder

d = Decoder()

# Client socket
# create an INET, STREAMing socket :
client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host_ip = '10.0.0.1'# Standard loopback interface address (localhost)
port = 10050 # Port to listen on (non-privileged ports are > 1023)
# now connect to the web server on the specified port number
client_socket.connect((host_ip,port))
#'b' or 'B'produces an instance of the bytes type instead of the str type
#used in handling binary data from network connections
data = b""
# Q: unsigned long long integer(8 bytes)
payload_size = struct.calcsize("!ff")

while True:


    data, addr = client_socket.recvfrom(1024) # buffer size is 1024 bytes

    if len(data) < 1:
        break

    d.byte_str_list.append(data)



d.decode()

client_socket.close()
