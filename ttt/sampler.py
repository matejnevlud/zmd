import numpy as np
from enum import Enum
from scipy.io import wavfile
import scipy.io.wavfile
import matplotlib.pyplot as plt

class Modulation(Enum):
    AMPLITUDE = 1
    FREQUENCY = 3


class Sampler:
    def __init__(self, wav_fname, modulation, samplerate):
        input_samplerate, data = scipy.io.wavfile.read('./sine.wav')
        self.samplerate = samplerate

        self.length = data / input_samplerate

        #self.x_axis = np.array([i / input_samplerate for i in range(data)])
        self.x_axis = np.arange(0, len(data) / input_samplerate, 1 / input_samplerate)
        self.y_axis = np.array([x / 32767.0 for x, _ in data])
        # self.y_axis = np.array([x for x, _ in data])
        self.y_axis.dtype = np.float

        if not type(modulation) == Modulation:
            raise Exception('bad modulation input')
        self.modulation = modulation

    def val_at(self, time):
        return np.interp(time, self.x_axis, self.y_axis)

    def iq_val_at(self, time):
        i = float(self.val_at(time))
        q = float(self.val_at(time + 1 / (4 * self.samplerate)))
        return i, q

    def sample(self):
        samples = []
        for i in range(int(self.length * self.samplerate)):
            samples.append(self.iq_val_at(i / self.samplerate))
        return samples

