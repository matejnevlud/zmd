import struct
from turtle import circle
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as w
import math
from matplotlib.patches import Circle, PathPatch
from mpl_toolkits.mplot3d import Axes3D 
import mpl_toolkits.mplot3d.art3d as art3d

class Decoder:
    def __init__(self):
        self.byte_str_list = []
        '''self.f2 = plt.figure()
        self.circles = []
        self.ax1 = self.f2.add_subplot(1,1,1,projection='3d')'''

    def decode(self):
        time = 0
        samplerate = 44100
        amplitudes = []
        i = 0 
        q = 0
        for b_str in self.byte_str_list:

            for x in range(128):
                if len(b_str) != 1024:
                    while len(b_str) != 1024:
                        b_str += '\x00'.encode('ascii')

                i, q = struct.unpack('!ff', b_str[x*8:x*8+8])
                # amplitudes.append(np.sqrt(i**2 + q**2))
                amplitudes.append(i*np.cos(2. * np.pi * samplerate * time) - q*np.sin(2. * np.pi * samplerate * time))
                time += 1 / samplerate
        
            '''circle_r = math.sqrt(i**2 + q**2)
            circle = Circle((0, 0), circle_r, edgecolor='red', facecolor=None, fill=False)
            self.ax1.add_patch(circle)
            self.circles.append(circle)
            art3d.pathpatch_2d_to_3d(circle, z=time*500, zdir='x')'''

        length = len(amplitudes) / samplerate


        time = np.linspace(0., length, len(amplitudes))
        
        '''self.ax1.set_xlim3d(0, 10000)
        self.ax1.set_ylim3d(-2, 2)
        self.ax1.set_zlim3d(-2, 2)'''

        plt.figure(2)
        plt.plot(time, amplitudes[:], label="Left channel")
        plt.legend()
        plt.xlabel("Time [s]")
        plt.ylabel("Amplitude")
        plt.show()

        amplitudes = np.array(amplitudes)
        amplitudes *= 32767

        w.write("example.wav", samplerate, amplitudes.astype(np.int16))






