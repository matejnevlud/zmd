#include "stdafx.h"

#define MARKER_BINS 7	// [-]
#define BIN_SIZE 8	// [px]
#define MARKER_SIZE ( MARKER_BINS * BIN_SIZE ) // [px]

const std::vector<cv::Point2f> MBAR::corners_ = { cv::Point( 0, 0 ),
	cv::Point( MARKER_SIZE - 1, 0 ), cv::Point( MARKER_SIZE - 1, MARKER_SIZE - 1 ),
	cv::Point( 0, MARKER_SIZE - 1 ) };

const std::vector<cv::Point3f> MBAR::corners_ws_ = { cv::Point3f( -3.0f, 3.0f, 0.0f ),
cv::Point3f( 3.0f, 3.0f, 0.0f ), cv::Point3f( 3.0f, -3.0f, 0.0f ), cv::Point3f( -3.0f, -3.0f, 0.0f ) };

const std::vector<cv::Point3f> MBAR::axis_ws_ = { cv::Point3f( 0.0f, 0.0f, 0.0f ),
cv::Point3f( 1.0f, 0.0f, 0.0f ), cv::Point3f( 0.0f, 1.0f, 0.0f ), cv::Point3f( 0.0f, 0.0f, 1.0f ) };

//http://www.mathaddict.net/hamming.htm
//http://homepages.bw.edu/~rmolmen/multimedia/hammingcode.swf
unsigned char encode_2bits( const unsigned char data2 )
{
	return 0;
}

inline unsigned char get_bit( const unsigned char byte, const char bit )
{
	assert( bit >= 0 && bit < 8 );

	return ( ( byte >> bit ) & 1 )/* == 1*/;
}

unsigned char decode_2bits( const unsigned char codeword5, bool & error )
{
	return 0;
}

MBAR::MBAR( const std::string & file_name )
{
	image_ = cv::imread( file_name );
}

void MBAR::start( const std::string & file_name )
{
	image_ = cv::imread( file_name );
	start();
}

template<typename type>
std::string to_binary( const type & value )
{
	std::bitset<sizeof( type ) * 8> bs( value );

	return bs.to_string();
}

void MBAR::start()
{


    cv::VideoCapture cap("../../../data/test_01.mp4");

    if(!cap.isOpened()){
        std::cout << "Error opening video stream or file" << std::endl;

    }

    while(1){

        contours_.clear();
        markers_.clear();

        cv::Mat frame;
        // Capture frame-by-frame
        cap >> image_;

        // If the frame is empty, break immediately
        if (image_.empty())
            break;

        preprocess_image();
        find_contours();
        crop_markers();
        read_markers();
        refine_corners();
        solve_pnp();
        // Display the resulting frame
        //imshow( "Frame", frame );

        // Press  ESC on keyboard to exit
        char c=(char)
                cv::waitKey(1);
        if(c==27)
            break;
    }

    // When everything done, release the video capture object
    cap.release();

    // Closes all the frames
    cv::destroyAllWindows();


}

void MBAR::preprocess_image()
{
    cvtColor( image_, image_gray_, cv::COLOR_BGRA2GRAY );
    //cv::imshow("grayscale", image_gray_);
    //cv::waitKey(0);

}

void MBAR::find_contours()
{
    cv::Mat image_thresholded;
    adaptiveThreshold( image_gray_, image_thresholded, 255.0, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 7, 3.0 );
    //cv::imshow("image_thresholded", image_thresholded);
    //cv::waitKey(0);

    std::vector<std::vector<cv::Point>> contours_candidates;
    cv::findContours(image_thresholded.clone(), contours_candidates, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    for (int i = 0; i < contours_candidates.size(); i++) {
        if (contours_candidates[i].size() < 4) {
            continue;
        }

        std::vector<cv::Point2f> approximated_contour;
        cv::approxPolyDP(contours_candidates[i], approximated_contour, 0.02 * arcLength(contours_candidates[i], true), true);

        if (cv::contourArea(approximated_contour) < 1000) {
            continue;
        }

        if (!cv::isContourConvex(approximated_contour)) {
            continue;
        }


        printf("Found matching contour %d\n", (int) i);

        std::vector<cv::Point2f> contour_ccw;
        cv::convexHull(approximated_contour, contour_ccw, false);


        if(contour_ccw.size() == 4)
            contours_.push_back(contour_ccw);
    }


    //! DEBUG IMAGE
    cv::Mat contour_debug = image_.clone();
    for (auto & contour : contours_) {
        for (int j = 0; j < contour.size(); j++) {
            cv::circle(contour_debug, contour[j], 3, cv::Scalar(255, 128, 0), -1);
        }
    }
    //cv::imshow("contour_debug", contour_debug);
    //cv::waitKey(0);


}

void MBAR::crop_markers()
{
    for(auto & contour : contours_) {
        cv::Mat marker;
        cv::Mat is2ms = cv::getPerspectiveTransform(contour, corners_);
        cv::warpPerspective(image_gray_, marker, is2ms, cv::Size(MARKER_SIZE, MARKER_SIZE));
        cv::threshold(marker, marker, 127.0, 255.0, cv::THRESH_OTSU);

        markers_.push_back(Marker(marker));
    }

    //! DEBUG IMAGE
    cv::Mat marker_debug = image_.clone();
    for (auto & marker : markers_) {
        //cv::imshow("marker_debug", marker.image);
        //cv::waitKey(0);
    }



}

int hamming(cv::Mat &flip_marker) {
    // Decode word
    int decoded_word = 0;
    for( int y = 1; y < flip_marker.cols - 1; y++){
        uchar p1 = flip_marker.at<uchar>(y, 1) == 255 ? 1 : 0;
        uchar p2 = flip_marker.at<uchar>(y, 2) == 255 ? 1 : 0;
        uchar d1 = flip_marker.at<uchar>(y, 3) == 255 ? 1 : 0;
        uchar p3 = flip_marker.at<uchar>(y, 4) == 255 ? 1 : 0;
        uchar d2 = flip_marker.at<uchar>(y, 5) == 255 ? 1 : 0;

        int wrong_bits = 0;

        if(p2 != d1) wrong_bits += 2;

        if(p3 != d2) wrong_bits += 4;

        if (p1 != ((d1 + d2) & 1)) wrong_bits += 1;

        // Repair damaged bits
        if(wrong_bits > 0) {
            if (wrong_bits == 3) { d1 = ~d1 & 1; }
            if (wrong_bits == 5) { d2 = ~d2 & 1; }
            if (wrong_bits > 5) { return -1; }
        }

        // Decode value sum
        decoded_word <<= 1;
        decoded_word |= d1;

        decoded_word <<= 1;
        decoded_word |= d2;
    }

    return decoded_word;
}

void MBAR::read_markers()
{
    cv::Mat orig_marker = cv::imread("../../../data/marker.png", cv::IMREAD_GRAYSCALE);
    cv::resize(orig_marker, orig_marker, cv::Size(MARKER_BINS, MARKER_BINS));
    cv::Mat flip_orig_marker = orig_marker.clone();

    for( int y = 1; y < orig_marker.cols - 1; y++)
        flip_orig_marker.at<uchar>(y, 1) = orig_marker.at<uchar>(y, 1) == 255 ? 0 : 255;

    int original_word = hamming(flip_orig_marker);

    cv::Mat m_rotation = cv::getRotationMatrix2D(cv::Point2f((MARKER_BINS - 1)*0.5f, (MARKER_BINS - 1)*0.5f), -90.0, 1.0);


    //cv::imshow("marker_orig", orig_marker);
    //cv::waitKey(0);


    std::vector<Marker> valid_markers;

    for (size_t i = 0; i < markers_.size(); i++)
    {
        cv::Mat marker;

        cv::resize(markers_[i].image, marker, cv::Size(MARKER_BINS, MARKER_BINS));
        cv::threshold(marker, marker, 127.0, 255.0, cv::THRESH_BINARY);

        for (size_t r = 0; r < 4; r++)
        {

            cv::warpAffine(marker, marker, m_rotation, marker.size());
            std::rotate(contours_[i].begin(), contours_[i].end() - 1, contours_[i].end());

            //cv::Mat btwxor(orig_marker.size(), CV_8UC1);
            //cv::bitwise_xor(marker, orig_marker, btwxor);
            //cv::imshow("marker_xord", btwxor);

            // Flip bits
            cv::Mat flip_marker = marker.clone();
            for( int y = 1; y < marker.cols - 1; y++)
                flip_marker.at<uchar>(y, 1) = marker.at<uchar>(y, 1) == 255 ? 0 : 255;


            //cv::imshow("flip_marker", flip_marker);
            //cv::waitKey(0);

            // Decode word
            int decoded_word = hamming(flip_marker);

            if(decoded_word == original_word) {
                //cv::imshow("flip_marker", flip_marker);
                //cv::waitKey(0);
                printf("Original value %d\n", original_word);
                printf("Decoded value %d\n", decoded_word);
                Marker valid(marker);
                valid.corners_is = contours_[i];
                valid_markers.push_back(valid);
                break;
            }

        }

        if (!valid_markers.empty()) break;
    }

    markers_ = valid_markers;

}

void MBAR::refine_corners()
{
    for (int i = 0; i < markers_.size(); i++)
    {
        cv::cornerSubPix(image_gray_, markers_[i].corners_is, cv::Size(5, 5),
                         cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::MAX_ITER, 30, 0.1));
    }
}

cv::Point2d w2is(cv::Point3d ws, cv::Mat m_M) {
    cv::Mat point_ws(4, 1, CV_64FC1);
    point_ws.at<double>(0, 0) = ws.x;
    point_ws.at<double>(1, 0) = ws.y;
    point_ws.at<double>(2, 0) = ws.z;
    point_ws.at<double>(3, 0) = 1;


    cv::Mat_<double> temp_ws(4/*rows*/,1 /* cols */);
    temp_ws(0,0)=ws.x;
    temp_ws(1,0)=ws.y;
    temp_ws(2,0)=ws.z;
    temp_ws(3,0)=1.0;

    cv::Mat point_is = m_M * temp_ws;
    point_is /= point_is.at<double>(2, 0);

    cv::Point2d point = cv::Point2d(point_is.at<double>(0, 0), point_is.at<double>(1, 0));
    return point;
}


void MBAR::solve_pnp()
{

	cv::Mat m_camera = camera_matrix();
	cv::Mat distortion_coefficients = cv::Mat::zeros( 4, 1, CV_64FC1 );
	double & k1 = distortion_coefficients.at<double>( 0, 0 );
	double & k2 = distortion_coefficients.at<double>( 1, 0 );
	double & p1 = distortion_coefficients.at<double>( 2, 0 );
	double & p2 = distortion_coefficients.at<double>( 3, 0 );
	k1 = 0; // radial distortion
	k2 = 0;
	p1 = 0; // tangential distortion
	p2 = 0;

    for (auto & marker : markers_)
    {
        cv::Mat rvec, tvec;
        cv::solvePnP(corners_ws_, marker.corners_is, m_camera, distortion_coefficients, rvec, tvec);
        cv::Mat m_R(3, 3, CV_64FC1);
        cv::Rodrigues(rvec, m_R);

        cv::Mat m_T;
        hconcat(m_R, tvec, m_T);
        cv::Mat m_M = m_camera * m_T;


        // Debug draw
        std::vector<cv::Point> corners_again;

        for (size_t j = 0; j < corners_ws_.size(); j++)
        {
            cv::Point2d point = w2is(corners_ws_[j], m_M);
            corners_again.push_back(point);
            cv::circle(image_, point, 5, cv::Scalar(64 * j, 64 * j, 64 * j), -1);
        }

        cv::fillPoly(image_, corners_again, cv::Scalar(128, 0, 128));

        // ORIGIN AXIS
        cv::Point2d center = w2is(axis_ws_[0], m_M);
        cv::Point2d x = (w2is(axis_ws_[1] * 2, m_M));
        cv::Point2d y = (w2is(axis_ws_[2] * 2, m_M));
        cv::Point2d z = (w2is(axis_ws_[3] * 2, m_M));

        cv::circle(image_, center, 5, cv::Scalar(0, 255, 0), -1);
        cv::arrowedLine(image_, center, x, cv::Scalar(0, 0, 255), 2);
        cv::arrowedLine(image_, center, y, cv::Scalar(0, 255, 0), 2);
        cv::arrowedLine(image_, center, z, cv::Scalar(255, 0, 0), 2);

        cv::imshow("image_", image_);
        //cv::waitKey(0);

    }

}

cv::Mat MBAR::camera_matrix() const
{
	cv::Mat camera_matrix = cv::Mat::zeros( 3, 3, CV_64FC1 );
	double & fx = camera_matrix.ptr<double>( 0 )[0];
	double & fy = camera_matrix.ptr<double>( 1 )[1];
	double & cx = camera_matrix.ptr<double>( 0 )[2];
	double & cy = camera_matrix.ptr<double>( 1 )[2];
	camera_matrix.ptr<double>( 2 )[2] = 1.0;
	const cv::Size size = image_.size();
	const double fov_y = DEG2RAD( 42.185 );
	fx = ( size.height * 0.5 ) / tan( fov_y * 0.5 );
	fy = fx;
	cx = size.width * 0.5;
	cy = size.height * 0.5;

	return camera_matrix;
}
