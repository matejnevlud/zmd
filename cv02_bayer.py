import cv2
import numpy as np
from PIL import Image, ImageOps

matrix = [
    ['R', 'G', 'R', 'G'],
    ['G', 'B', 'G', 'B'],
    ['R', 'G', 'R', 'G'],
    ['G', 'B', 'G', 'B'],
]

def get_channel(x, y):
    # Get relative index in matrix
    x_m = (x) % 4
    y_m = (y) % 4

    # Determine which channel do i have
    channel = matrix[x_m][y_m]

    return channel

with Image.open("bayer.bmp") as image:
    image = ImageOps.grayscale(image)
    image.show()

    test = Image.new('RGB', (11, 11))
    demosaic = Image.new('RGB', image.size)
    for x in range(0 + 1, image.width - 1):
        for y in range(0 + 1, image.height - 1):

            # Interpolate missing channels
            R_buffer = []
            G_buffer = []
            B_buffer = []

            # Get surrounding pixels
            for i in range(x - 1, x + 2):
                for j in range(y - 1, y + 2):
                    neighbor_channel = get_channel(i, j)
                    if neighbor_channel == 'R':
                        R_buffer.append(image.getpixel((i, j)))
                    elif neighbor_channel == 'G':
                        G_buffer.append(image.getpixel((i, j)))
                    elif neighbor_channel == 'B':
                        B_buffer.append(image.getpixel((i, j)))

            R = sum(R_buffer) / len(R_buffer)
            G = sum(G_buffer) / len(G_buffer)
            B = sum(B_buffer) / len(B_buffer)


            demosaic.putpixel((x, y), (int(R), int(G), int(B)))

    demosaic.show()









image = cv2.imread("./bayer.bmp", cv2.IMREAD_GRAYSCALE)
cv2.imshow("image", image)

#image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


# BAYER DEMOSAICING
demosaic = np.zeros([image.shape[0], image.shape[1], 3], dtype="uint8")
for y in range(0 + 1, image.shape[0] - 1):
    for x in range(0 + 1, image.shape[1] - 1):
        uchar_pixel = image[y, x]
        #print((x, y), uchar_pixel)

        # Get relative index in matrix
        x_m = x % 5
        y_m = y % 5


        # Determine which channel do i have
        channel = matrix[y_m][x_m]


        # Interpolate missing channels
        R = 0
        G = 0
        B = 0

        if channel == 'R':
            R = uchar_pixel
            G = (image[y - 1, x] + image[y + 1, x] + image[y, x - 1] + image[y, x + 1]) / 4
            B = (image[y - 1, x - 1] + image[y - 1, x + 1] + image[y + 1, x - 1] + image[y + 1, x + 1]) / 4
        elif channel == 'G':
            R = (image[y - 1, x] + image[y + 1, x]) / 2
            G = uchar_pixel
            B = (image[y, x - 1] + image[y, x + 1]) / 2
        elif channel == 'B':
            R = (image[y - 1, x] + image[y + 1, x] + image[y, x - 1] + image[y, x + 1]) / 4
            G = (image[y - 1, x - 1] + image[y - 1, x + 1] + image[y + 1, x - 1] + image[y + 1, x + 1]) / 4
            B = uchar_pixel

        demosaic[y, x] = ( int(B), int(G), int(R) )


cv2.imshow("demosaic", demosaic)
cv2.waitKey(0)
