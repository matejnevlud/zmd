import os
import scipy.io
import scipy.io.wavfile
import numpy as np
import numpy.linalg as LA
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.signal import hilbert, savgol_filter
import cmath


fs = 44100 # sampling frequency, Hz
fc = 80  # carrier frequency, Hz
fm = 2  # modulation frequency, Hz

T = 1 # seconds

t = np.arange(0, T, 1/fs)

# Produce ramp from 0 to 1
beta = 20

SIGNAL_IN = np.sin(2 * np.pi * fm * t)
carrier = np.sin(2 * np.pi * fc * t)
FM = np.sin(2 * np.pi * fc * t + beta * SIGNAL_IN)

I = np.real(hilbert(FM))
Q = np.imag(hilbert(FM))


# Demodulate
analyticSignal = I + 1j * Q
baseband = analyticSignal * np.exp(-2*np.pi*fc*t*1j); # complex mixing
SIGNAL_OUT = np.angle( baseband[1::1] * np.conjugate(baseband[0:-1:1]) ); # fm demod

# 2 method
phi_s = np.unwrap(np.angle(I + 1j * Q));
SIGNAL_OUT = np.diff(phi_s)/(2*np.pi*1/fs);

plt.subplot(5,1,1)
plt.title('Amplitude Modulation')
plt.plot(np.cos(2 * np.pi * fm * t),'b')
plt.ylabel('Amplitude')
plt.xlabel('Message signal')

plt.subplot(5,1,2)
plt.title('Amplitude Modulation')
plt.plot(carrier,'r')
plt.ylabel('Amplitude')
plt.xlabel('Message signal')

plt.subplot(5,1,3)
plt.title('Amplitude Modulation')
plt.plot(I,'g')
plt.ylabel('Amplitude')
plt.xlabel('Message signal')

plt.subplot(5,1,4)
plt.title('Amplitude Modulation')
plt.plot(Q,'g')
plt.ylabel('Amplitude')
plt.xlabel('Message signal')

plt.subplot(5,1,5)
plt.title('Amplitude Modulation')
plt.plot(SIGNAL_OUT,'g')
plt.ylabel('Amplitude')
plt.xlabel('Message signal')

plt.subplots_adjust(hspace=1)
plt.rc('font', size=15)
fig = plt.gcf()
fig.set_size_inches(16, 9)


plt.show()