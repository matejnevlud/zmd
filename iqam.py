import os
import scipy.io
import scipy.io.wavfile
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert


sampleRate, audioBuffer = scipy.io.wavfile.read('./star.wav')

# normalize audio
audioBuffer = audioBuffer / np.max(np.abs(audioBuffer))

duration = len(audioBuffer) / sampleRate
time = np.arange(0, duration, 1 / sampleRate)

# upsample audio
newTime = np.linspace(0, duration, len(audioBuffer) * 10)
buffer = np.interp(newTime, time, audioBuffer)

carrier = 1.0 * np.cos(2.0 * np.pi * 8000.0 * newTime)

# create modulated signal
AM = carrier * (1 + buffer)
I = np.real(hilbert(AM))
#Q = np.append(I[21:], np.zeros(21))
Q = np.imag(hilbert(AM))




# demodulate I and Q AM signal
IQ = ((I * I + Q * Q) ** 0.5) - 1.0

scipy.io.wavfile.write("iq.wav", sampleRate * 10, IQ)

plt.subplot(5,1,1)
plt.title('Amplitude Modulation')
plt.plot(buffer,'g')
plt.ylabel('Amplitude')
plt.xlabel('Message signal')

plt.subplot(5,1,2)
plt.plot(carrier, 'r')
plt.ylabel('Amplitude')
plt.xlabel('Carrier signal')

plt.subplot(5,1,3)
plt.plot(I, color="purple")
plt.ylabel('Amplitude')
plt.xlabel('I signal')

plt.subplot(5,1,4)
plt.plot(Q, color="purple")
plt.ylabel('Amplitude')
plt.xlabel('Q signal')

plt.subplot(5,1,5)
plt.plot(IQ, color="purple")
plt.ylabel('Amplitude')
plt.xlabel('I*Q signal')

plt.subplots_adjust(hspace=1)
plt.rc('font', size=15)
fig = plt.gcf()
fig.set_size_inches(16, 9)


plt.show()