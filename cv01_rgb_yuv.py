import cv2
import numpy as np

image = cv2.imread("./barn.png")
image = cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
cv2.imshow("image", image)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)


rgb_to_yuv2 = np.array([
    [0.299, -0.147, 0.615],
    [0.587, -0.289, -0.515],
    [0.114, 0.436, -0.100]
])

yuv_to_rgb2 = np.array([
    [1.0, 1.0, 1.0],
    [0.0, -0.397, 2.034],
    [1.137, -0.580, 0.0]
])


# YUV IMAGE CONVERSION
yuv = np.zeros(image.shape, dtype=np.float32)
for y in range(0, image.shape[0]):
    for x in range(0, image.shape[1]):
        uchar_pixel = image[y, x]
        yuv[y, x] = rgb_to_yuv2.dot(np.array(uchar_pixel))
        #yuv[y, x] = [yuv[y, x][0], yuv[y, x][1] + 0.5, yuv[y, x][2]]



cv2.imshow("Y", yuv[:,:,0])
cv2.imshow("U", yuv[:,:,1])
cv2.imshow("V", yuv[:,:,2])
#cv2.imshow("YUV", yuv)
#cv2.imshow("TEST",  cv2.cvtColor(image, cv2.COLOR_RGB2YUV))


# RGB IMAGE CONVERSION
rgb = yuv.copy()
for y in range(0, yuv.shape[0]):
    for x in range(0, yuv.shape[1]):
        # threshold the pixel
        rgb[y, x] = yuv_to_rgb2.dot(np.array(yuv[y, x]))

cv2.imshow("BGR", cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR))
cv2.waitKey(0)