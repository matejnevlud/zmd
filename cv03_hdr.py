import math

import cv2
import numpy as np
from PIL import Image, ImageOps


s0 = Image.open("./hdr/s1_0.png").convert("RGB")
s1 = Image.open("./hdr/s1_1.png").convert("RGB")
s2 = Image.open("./hdr/s1_2.png").convert("RGB")
s3 = Image.open("./hdr/s1_3.png").convert("RGB")
s4 = Image.open("./hdr/s1_4.png").convert("RGB")
sdr_images = [s0, s1, s2, s3, s4]


hdr = s0.copy()

mikro = 0.5
sigma = 0.3

for x in range(0, hdr.width):
    for y in range(0, hdr.height):
        w_k_array = []

        for k in range(0, 5):
            rgb_arr = []
            for i in range(0, 3):
                I_k_i_j = sdr_images[k].getpixel((x, y))[i]
                w_k = math.exp( - math.pow(I_k_i_j - (255.0 * mikro), 2) / (2 * math.pow(255.0 * sigma, 2)) )
                rgb_arr.append(w_k)
            w_k_array.append(rgb_arr)


        sum_w_k_r, sum_w_k_g, sum_w_k_b = 0, 0, 0
        for k in range(0, 5):
                sum_w_k_r += w_k_array[k][0]
                sum_w_k_g += w_k_array[k][1]
                sum_w_k_b += w_k_array[k][2]

        for k in range(0, 5):
                w_k_array[k][0] /= sum_w_k_r
                w_k_array[k][1] /= sum_w_k_g
                w_k_array[k][2] /= sum_w_k_b

        R_ij = [0, 0, 0]
        for k in range(0, 5):
            for i in range(0, 3):
                R_ij[i] += w_k_array[k][i] * sdr_images[k].getpixel((x, y))[i]


        print(R_ij)

        hdr.putpixel((x, y), (int(R_ij[0]), int(R_ij[1]), int(R_ij[2])))




hdr.show()
