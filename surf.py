import cv2
import numpy as np

img = cv2.imread("./0000023720.jpg")
#image = cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

#img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

orb = cv2.ORB_create()

# find the keypoints with ORB
keypoints = orb.detect(img, None)

# compute the descriptors with ORB
keypoints, descriptors = orb.compute(img, keypoints)

img2 = cv2.drawKeypoints(img, keypoints, None, color=(0, 0, 255), flags=4)

cv2.imshow("image", img2)
cv2.waitKey(0)