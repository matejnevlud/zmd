import cv2
import numpy as np
import sys
import math
import multiprocessing


def sad(img_l, img_r, y, x, w=3, max_disp=64):
    differences = []
    # for x_r in range(max(x_l - max_disp, 0), min(x_l, img_r.shape[1])):
    for d in range(0, max_disp):

        R = int(math.floor(w / 2.0))
        kernel_sum = 0
        for v in range(-R, R + 1):
            for u in range(-R, R + 1):
                try:
                    kernel_sum += abs(img_r[y + v, x + u - d] - img_l[y + v, x + u])
                except:
                    None

        try:
            differences.append(kernel_sum)
        except:
            differences.append(max(differences))

    try:
        return np.argmin(differences)
    except:
        # print(differences, 'error')
        return 0


def hamming(str1, str2):
    i = 0
    count = 0

    while(i < len(str1)):
        if(str1[i] != str2[i]):
            count += 1
        i += 1
    return count

def census(img_l, img_r, y, x, w=3, max_disp=64):
    differences = []

    for d in range(0, max_disp):

        R = int(math.floor(w / 2.0))
        l_bitstring = ''
        r_bitstring = ''
        for v in range(-R, R + 1):
            for u in range(-R, R + 1):
                try:
                    l_bitstring += '1' if img_l[y + v, x + u] < img_l[y, x] else '0'
                    r_bitstring += '1' if img_r[y + v, x + u - d] < img_r[y, x - d] else '0'
                except:
                    None

        try:
            differences.append(hamming(r_bitstring, l_bitstring))
        except:
            differences.append(max(differences))


    try:
        return np.argmin(differences)
    except:
        # print(differences, 'error')
        return 0



def rank_smaller_map(img_l, img_r, w=3):

    rank_map_l = np.zeros(img_l.shape, np.int8)
    rank_map_r = np.zeros(img_r.shape, np.int8)


    R = int(math.floor(w / 2.0))

    for y in range(R, img_l.shape[0] - R):
        for x in range(R, img_l.shape[1] - R):
            for u in range(-R, R + 1):
                for v in range(-R, R + 1):
                    try:
                        rank_map_l[y, x] += 1 if img_l[y + v, x + u] < img_l[y, x] else 0
                        rank_map_r[y, x] += 1 if img_r[y + v, x + u] < img_r[y, x] else 0
                    except:
                        None

            print('Transforming rank map', int((y * img_l.shape[1] + x) / (img_l.shape[0] * img_l.shape[1]) * 100), '%', end='\r')

    return rank_map_l, rank_map_r


def ncc_mean_map(img_l, img_r, w=3):

    mean_map_l = np.zeros(img_l.shape, np.int8)
    mean_map_r = np.zeros(img_r.shape, np.int8)


    R = int(math.floor(w / 2.0))

    for y in range(R, img_l.shape[0] - R):
        for x in range(R, img_l.shape[1] - R):
            mean_map_l[y, x] = np.mean(img_l[y - R:y + R + 1, x - R:x + R + 1])
            mean_map_r[y, x] = np.mean(img_r[y - R:y + R + 1, x - R:x + R + 1])

            print('Transforming mean map', int((y * img_l.shape[1] + x) / (img_l.shape[0] * img_l.shape[1]) * 100), '%', end='\r')

    return mean_map_l, mean_map_r

def ncc(img_l, img_r, y, x, w, max_disp, mean_l, mean_r):
    differences = []
    # for x_r in range(max(x_l - max_disp, 0), min(x_l, img_r.shape[1])):
    for d in range(0, max_disp):

        R = int(math.floor(w / 2.0))

        top = 0
        bottom = 0
        for v in range(-R, R + 1):
            for u in range(-R, R + 1):
                try:
                    top += (img_l[y + v, x + u] - mean_l[y, x]) * (img_r[y + v, x + u - d] - mean_r[y, x - d])
                    bottom += math.pow((img_l[y + v, x + u] - mean_l[y, x]), 2) * math.pow((img_r[y + v, x + u - d] - mean_r[y, x - d]), 2)
                except:
                    None

        try:
            differences.append(top / math.sqrt(bottom))
        except:
            differences.append(min(differences))

    try:
        return np.argmax(differences)
    except:
        # print(differences, 'error')
        return 0


# ./stereo img_l img_r max_disp metric window_size img_output
if __name__ == '__main__':
    args = sys.argv[1:]

    if len(args) != 6:
        print("You must specify correct parameters: rows cols image_width image_height input_video output_image")
        exit(0)

    try:
        img_l, img_r, max_disp, metric, window_size, img_output = args
        window_size = int(window_size.split('x')[0])
        max_disp = int(max_disp)

        img_l = cv2.imread(img_l, cv2.IMREAD_GRAYSCALE).astype('int32')
        img_r = cv2.imread(img_r, cv2.IMREAD_GRAYSCALE).astype('int32')
    except:
        print("You must specify correct parameters")
        exit(0)

    done_progress = []
    stereo_img = np.zeros(img_l.shape, np.uint8)
    # calculate mean map for ncc algorithm
    mean_l, mean_r = ncc_mean_map(img_l, img_r, w=window_size)
    # apply rank transform
    rank_l, rank_r = rank_smaller_map(img_l, img_r, w=window_size)

    for y in range(0, img_l.shape[0]):
        for x in range(0, img_l.shape[1]):

            if metric == 'sad':
                disp = sad(img_l, img_r, y, x, window_size, max_disp)
            elif metric == 'census':
                disp = census(img_l, img_r, y, x, window_size, max_disp)
            elif metric == 'ncc':
                disp = ncc(img_l, img_r, y, x, window_size, max_disp, mean_l, mean_r)
            elif metric == 'rank':
                disp = sad(rank_l, rank_r, y, x, window_size, max_disp)


            stereo_img[y, x] = disp * 4

            progress = int((y * img_l.shape[1] + x) / (img_l.shape[0] * img_l.shape[1]) * 100)
            print('Calculating', metric, progress, '%', disp, end='\r')

            if int(progress) not in done_progress:
                done_progress.append(int(progress))
                cv2.imshow(str('stereo_img'), stereo_img)
                cv2.waitKey(2)


    cv2.imshow(str('stereo_img'), stereo_img)
    cv2.waitKey(0)
    cv2.imwrite(img_output, stereo_img)
    print("Done")
