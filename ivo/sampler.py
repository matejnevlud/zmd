import numpy as np
from enum import Enum
from scipy.io import wavfile
import matplotlib.pyplot as plt
from scipy.signal import hilbert, savgol_filter

class Modulation(Enum):
    AMPLITUDE = 1
    FREQUENCY = 3


class Sampler:
    def __init__(self, wav_fname, modulation, samplerate):
        if not type(modulation) == Modulation:
            raise Exception('bad modulation input')
        self.modulation = modulation

        input_samplerate, data = wavfile.read(wav_fname)
        self.samplerate = samplerate

        self.length = data.shape[0] / input_samplerate
        self.x_axis = np.array([i / input_samplerate for i in range(data.shape[0])])

        self.y_axis = np.array([x / 32767.0 for x, _ in data])
        self.y_axis.dtype = np.float

        if self.modulation == Modulation.FREQUENCY:
            fc = 1000  # carrier frequency, Hz
            self.y_axis = np.sin(2 * np.pi * fc * self.x_axis + self.y_axis)

    def val_at(self, time):
        return np.interp(time, self.x_axis, self.y_axis)

    def iq_val_at(self, time):
        i = float(self.val_at(time))
        q = float(self.val_at(time + 1 / (4 * self.samplerate)))
        return i, q

    def sample(self):
        if self.modulation == Modulation.FREQUENCY:
            I = np.real(hilbert(self.y_axis))
            Q = np.imag(hilbert(self.y_axis))

            return [(I[x], Q[x]) for x in range(len(I))]
        else:
            return [self.iq_val_at(t) for t in np.arange(0.0, self.length, 1/self.samplerate)]



