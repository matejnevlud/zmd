import socket
import struct
from sampler import *

#data = Sampler('input.wav', Modulation.AMPLITUDE, 44100).sample()
data = Sampler('input.wav', Modulation.FREQUENCY, 44100).sample()

server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host_name  = socket.gethostname()
host_ip = socket.gethostbyname(host_name)
print('HOST IP:',host_ip)
port = 11117
socket_address = (host_ip,port)
print('Socket created')
server_socket.bind(socket_address)
print('Socket bind complete')
server_socket.listen(5)
print('Socket now listening')

message = b''
while True:
    client_socket, addr = server_socket.accept()
    print('Connection from:', addr)
    if client_socket:
        message = b''
        for n, (i, q) in enumerate(data):
            message += struct.pack("!ff", i, q)
            if n % 128 == 127:
                client_socket.sendall(message)
                message = b''
        client_socket.sendall(b'0')
        client_socket.close()
