import socket
import cv2
import struct
from decoder import Decoder
from sampler import Modulation
import pygame
import numpy as np
import math


#d = Decoder(Modulation.AMPLITUDE)
d = Decoder(Modulation.FREQUENCY)

client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host_ip = '158.196.237.174'# Standard loopback interface address (localhost)
port = 11117
client_socket.connect((host_ip,port))
data = b""
payload_size = struct.calcsize("!ff")

while True:
    data, addr = client_socket.recvfrom(1024) # buffer size is 1024 bytes
    if len(data) < 1:
        break
    d.byte_str_list.append(data)

d.decode()
client_socket.close()

size = 1000, 1000, 3
image = np.zeros(size, dtype=np.uint8)

pygame.mixer.init()

sound = pygame.mixer.Sound('example.wav')
sound_length = int(sound.get_length()*1000)
prevRad = 0
cur_ms = 0
skip_ms = int(len(d.iq_vector_list)/(sound_length))

pygame.mixer.music.load('example.wav')
pygame.mixer.music.play(0)

print(sound_length)


while pygame.mixer.music.get_busy():
    cur_ms = pygame.mixer.music.get_pos()
    #print(cur_ms)
    index = cur_ms*skip_ms
    rad = int(math.ceil(d.iq_vector_list[index]/4)) ### potřebujeme nejvyšší hodnoty max v řádech stovek, aby se to vešlo na monitor
    #print(rad)
    cv2.circle(image, (500, 500), prevRad, (0, 0, 0), 1)

    iamge = cv2.circle(image, (500, 500), rad, (0, 0, 255), 1)
    prevRad = rad
    cv2.imshow('image',image)
    cv2.waitKey(10)

pygame.time.wait(1000) ### vykreslení trvá o fous méně kvůli zaokrouhlení, tak ať se neusekne písnička těsně před koncem

