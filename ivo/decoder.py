import struct
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as w
import math
from sampler import Modulation

class Decoder:
    def __init__(self, modulation):
        if not type(modulation) == Modulation:
            raise Exception('bad modulation input')
        self.modulation = modulation

        self.byte_str_list = []
        self.iq_vector_list = []

    def decode(self):
        time = 0
        samplerate = 44100
        amplitudes = []
        i = 0 
        q = 0
        I = []
        Q = []
        t= []
        for b_str in self.byte_str_list:
            for x in range(128):
                if len(b_str) != 1024:
                    while len(b_str) != 1024:
                        b_str += '\x00'.encode('ascii')

                i, q = struct.unpack('!ff', b_str[x*8:x*8+8])

                i_plus_q = i**2 + q**2
                iq_sqrt = math.sqrt(i_plus_q)
                

                if self.modulation == Modulation.AMPLITUDE:
                    self.iq_vector_list.append(iq_sqrt*10000)
                    amplitudes.append(i*np.cos(2. * np.pi * samplerate * time) - q*np.sin(2. * np.pi * samplerate * time))
                else:
                    self.iq_vector_list.append(iq_sqrt*1000)
                    I.append(i)
                    Q.append(q)
                    t.append(time)
                    #print(i)

                time += 1 / samplerate
        if self.modulation == Modulation.FREQUENCY:
            fc = 1000
            I = np.array(I)
            Q = np.array(Q)
            t = np.array(t)

            analyticSignal = I + 1j * Q
            baseband = analyticSignal * np.exp(-2 * np.pi * fc * t * 1j)
            amplitudes = np.angle(baseband[1::1] * np.conjugate(baseband[0:-1:1]))


        length = len(amplitudes) / samplerate
        time = np.linspace(0., length, len(amplitudes))

        plt.figure(2)
        plt.plot(time, amplitudes[:], label="Left channel")
        plt.legend()
        plt.xlabel("Time [s]")
        plt.ylabel("Amplitude")
        plt.show()

        amplitudes = np.array(amplitudes)
        amplitudes *= 32767

        w.write("example.wav", samplerate, amplitudes.astype(np.int16))






